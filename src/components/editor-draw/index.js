import { withInstall } from '@jecloud/ui/src/utils/index';
import JeEditorDraw from './src/editor-draw';
import './style/index'
const EditorDraw = withInstall(JeEditorDraw);

export default EditorDraw;
