import { defineAsyncComponent } from 'vue';

export const FORM_COMPONENTS = {
  /**
   * 基础
   */
  base: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/views/form/form-base/index.vue'),
  }),

  /**
   * 开始节点
   */
  start: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/views/form/form-start/index.vue'),
  }),

  /**
   * 结束节点
   */
  end: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/views/form/form-end/index.vue'),
  }),

  /**
   * 连线
   */
  line: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/views/form/form-line/index.vue'),
  }),

  node: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/views/form/form-node/index.vue'),
  }),

  /**
   * 分支聚合节点
   */
  ForkJoin: defineAsyncComponent({
    delay: 1000,
    loader: () => import('@/views/form/form-fork-join/index.vue'),
  }),
};
