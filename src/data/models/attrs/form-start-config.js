export default class FormStartConfig {
  constructor(options) {
    /**
     * 任何人启动
     */
    this.canEveryoneStart = options.canEveryoneStart || '0';

    /**
     * 可启动角色Id
     */
    this.canEveryoneRolesId = options.canEveryoneRolesId || '';

    /**
     * 可启动角色名称
     */
    this.canEveryoneRolesName = options.canEveryoneRolesName || '';

    /**
     * 表达式
     */
    this.startExpression = options.startExpression || '';

    /**
     * 表达式Fn
     */
    this.startExpressionFn = options.startExpressionFn || '';
  }
}
