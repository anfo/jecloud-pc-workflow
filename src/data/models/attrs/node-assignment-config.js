export default class NodeAssignmentConfig {
  constructor(options) {
    /**
     * 规则列表数据
     */
    this.resource = options.resource || [];

    /**
     * 人员参照编码
     */
    this.referToCode = options.referToCode || 'LOGUSER';
  }
}
