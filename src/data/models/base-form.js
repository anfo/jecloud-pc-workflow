import FormBaseConfig from './attrs/form-base-config';
import FormStartConfig from './attrs/form-start-config';
import FormExtendConfig from './attrs/form-extend-config';
import FormInformRemind from './attrs/form-inform-remind';

export default class BaseForm {
  constructor(options) {
    /**
     * 主键
     */
    this.JE_WORKFLOW_PROCESSINFO_ID = options.JE_WORKFLOW_PROCESSINFO_ID || '';

    /**
     * 流程的key，前端可以根据中文名称自动生成
     */
    this.process_id = options.process_id || '';

    /**
     * 工作流名称
     */
    this.name = options.name;

    /**
     *  基础数据
     */
    this.processBasicConfig = new FormBaseConfig(options.processBasicConfig || {});

    /**
     * 启动配置
     */
    this.startupSettings = new FormStartConfig(options.startupSettings || {});

    /**
     * 扩展配置
     */
    this.extendedConfiguration = new FormExtendConfig(options.extendedConfiguration || {});

    /**
     * 事件配置
     * [{
		  	"type_name(事件类型)": "",
				"type_code(事件类型)": "",
				"executionStrategy_name(执行策略)": "",
				"executionStrategy_code(执行策略)": "",
				"assignmentFieldConfiguration（赋值字段配置）": "",
				"serviceName (业务bean)": "",
				"method(业务方法)": "",
				"existenceParameter(是否存在参数)": true
		  }]
     */
    this.customEventListeners = options.customEventListeners || [];

    /**
     * 通知提醒
     */
    this.messageSetting = new FormInformRemind(options.messageSetting || {});

    /**
     * 按钮配置
     * [
     *  {
          "buttonId (写死)": "startBtn",
          "buttonCode (写死)": "startBtn",
          "buttonName(写死),": "启动",
          "customizeName(自定义名称)": "启动",
          "customizeComments(自定义审批意见)": "启动流程",
          "operationId(写死)": "processEmptyStartOperation",
          "backendListeners（后端脚本）": null,
          "appListeners(app脚本)": null,
          "pcListeners(pc脚本)": null,
          "displayWhenNotStarted(写死)": true,
          "displayExpression(写死)": "startBtn"
		    }
     * ]
     */
    this.processButtonConfig = options.processButtonConfig || [];
  }
}
