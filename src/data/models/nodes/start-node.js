import BaseNode from '../base-node';
import { isNotEmpty } from '@jecloud/utils';

export default class StartNode extends BaseNode {
  constructor(options) {
    super(options);
    /**
     * 基础配置
     */
    this.properties = {
      //id，不会重复的唯一id
      resourceId: isNotEmpty(options.properties) ? options.properties.resourceId : 'start',
      //名称
      name: isNotEmpty(options.properties) ? options.properties.name : '',
      //绑定表单名称
      formSchemeName:
        isNotEmpty(options.properties) && isNotEmpty(options.properties.formSchemeName)
          ? options.properties.formSchemeName
          : '',
      //绑定表单id
      formSchemeId:
        isNotEmpty(options.properties) && isNotEmpty(options.properties.formSchemeId)
          ? options.properties.formSchemeId
          : '',
    };
  }
}
