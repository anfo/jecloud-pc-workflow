import BaseNode from '../base-node';
import { isNotEmpty } from '@jecloud/utils';
export default class ForkNode extends BaseNode {
  constructor(options) {
    super(options);

    /**
     * 基础配置
     */
    this.properties = {
      //id，不会重复的唯一id
      resourceId: isNotEmpty(options.properties) ? options.properties.resourceId : '',
      //名称
      name: isNotEmpty(options.properties) ? options.properties.name : '',
    };
  }
}
